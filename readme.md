# DapperKeeper
DapperKeeper is a wrapper around [Dapper](https://github.com/StackExchange/Dapper) to facilitate CRUD operations - mostly reflective type-based insert operations.

## Why?
I wrote DapperKeeper while developing the backend for a simple internal web application. I wanted to learn more about LINQ, reflection, and C#'s type system - so "what if I could use Dapper but automatically generate inserts" seemed like an interesting problem to tackle.

## What's Included Here
* A solution that contains the Data and Tests projects
	* Data is a data access layer
	* Tests is a MSTest unit test project that provides test coverage to the Data project
* I removed all of the application-specific Models and Repositories except for the assets related to `GoalAction`, to provide an example of how DapperKeeper might be used
	* DapperKeeper was built as part of a small web application that managed corporate goal-tracking - a `GoalAction` was the definition of a "goal", such as "Reduce operating costs by 5%" and included details such as the goal's objective, owner, outcome, etc.

## Getting Started
1. Clone the repo
2. Create a new empty database in Microsoft SQL Server with all defaults
3. Change [this line](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Tests/SqlTests.cs?at=master&fileviewer=file-view-default#SqlTests.cs-18) to a valid connection string within your environment
4. Build and run all tests! Step through some of them in the debugger to see what's going on inside

## Points of Interest
* When I started developing this project, I started by designing the database. Then, I built all unit tests around the development environment's version of the database, including setting up the database from its create script on every execution of the unit test suite. I have more experience with SQL development than I do with creating mocks / fakes / etc and I needed to be able to iterate quickly, and this is the pattern that evolved from that.
	* Database create script: [Data / SQL / CreateTables.sql](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Sql/CreateTables.sql?at=master&fileviewer=file-view-default)
	* Data integrity is managed via constraints / triggers / etc - the database throws errors that bubble up to the application. This was faster to develop than trying to manage integrity in a business layer.
		* Example: [dbo.TRG_LDAPUsers_Verify](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Sql/CreateTables.sql?at=master&fileviewer=file-view-default#CreateTables.sql-99): in order to build a recursive hierarchy of users from this table, only one user can have a `ManagerLoginName` of `NULL` (i.e., only one user can be the 'root' of the CTE that powers [dbo.V_LDAPUsers](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Sql/CreateTables.sql?at=master&fileviewer=file-view-default#CreateTables.sql-398)).
* Models have to inherit from [BaseModel](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Models/BaseModel.cs?at=master&fileviewer=file-view-default), which provides several additional fields to make the magic happen.
* Properties on models can have [custom attributes](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Attributes/?at=master) that tell DapperKeeper what to do with each property when translating it to an Insert statement.
	* For instance, [DefaultAttribute](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Attributes/DefaultAttribute.cs?at=master&fileviewer=file-view-default) tells the wrapper to just let the database provide the default value for a given property's column - for example, a `DateCreated` column might be defined as `DateCreated DATETIME NOT NULL DEFAULT GETDATE()`
* The real magic happens in [Data / Repositories / BaseRepository](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Repositories/BaseRepository.cs?at=master&fileviewer=file-view-default). All repositories must inherit from `BaseRepository` and are initialized with a type parameter.
	* This allows you to define a repository based around a model's type, and then call `myRepository.Insert(someListOfObjects);`
	* But in some cases, you can perform database operations on other types, as long as you provide a type parameter - even if `myRepository` is an instance of `TypeX`, you can do stuff like this: `string s = myRepository<string>("SELECT 'HelloWorld'");`
	* Insert reflects against the type and generates SQL to perform the insert: [check this out](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Repositories/BaseRepository.cs?at=master&fileviewer=file-view-default#BaseRepository.cs-91)
	* I wanted a nice way to manage selecting scalar values without having to call `.First()` explicitly on every list returned from a select statement and trapping an exception if there was no value when one was expected, so I wrote a method called [SelectScalar](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Repositories/BaseRepository.cs?at=master&fileviewer=file-view-default#BaseRepository.cs-74) and a matching [ScalarNotFoundException](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/Exceptions/ScalarNotFoundException.cs?at=master&fileviewer=file-view-default) to throw if no value was found
* [Data / DatabaseContext](https://bitbucket.org/jmpavlick/dapperkeeper/src/998b2ccd204d667ede3776b513635b2c35ea263c/Data/DatabaseContext.cs?at=master&fileviewer=file-view-default) manages the connection string for the application. It should be set during startup and populated from a Web.config if used in an ASP.NET MVC project. This allowed me to configure the database the same way in my web project as I did in my unit tests.
	* It could be implemented as a `readonly` property, but earlier in the development process all of my tests were managing their own setup and teardown, and since they could run in any order (and since the connection string was copied around various parts of the test project) I wanted it to be a little more lenient and allow the `ConnectionString` value to be set multiple times, as long as it was set to the same thing every time. This could be refactored.

## Disclaimer
DapperKeeper is probably not the best possible solution of all possible solutions for this kind of thing. There are plenty of other, better, faster, more mature ORMs out there - but I wanted to go through the process of building some of my own ORM code, as a learning exercise to get a better understanding of the complexities and challenges involved.

Reflection is computationally expensive and I would not recommend this pattern as a starting point for an application that needs to process hundreds of thousands of concurrent requests, because better solutions to this particular problem are available. While "can" does not imply "should", this was an excellent learning experience for me.

