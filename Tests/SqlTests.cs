﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoaltenderWeb.Data.Repositories;
using GoaltenderWeb.Data.Models;

namespace GoaltenderWeb.Data.Tests
{
	[TestClass]
	public class SqlTests
	{
		[TestInitialize]
		public void Execute_CreateTables_SQLScript()
		{
			DatabaseContext.ConnectionString = @"Server=JPAVLICK-THINKPAD\SQLEXPRESS; Database=Goaltender; User Id=SvcGoaltenderTemp; Password=Evans1929";

			string script = File.ReadAllText(Path.Combine(Assembly.GetExecutingAssembly().Location, @"..\..\..\..\Data\SQL\CreateTables.sql"));
			using (SqlConnection conn = new SqlConnection(DatabaseContext.ConnectionString))
			{
				conn.Open();
				foreach (string scriptlet in script.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries))
				{
					SqlCommand cmd = new SqlCommand(scriptlet);
					cmd.Connection = conn;
					cmd.ExecuteNonQuery();
				}

				conn.Close();
			}
		}
	}
}
