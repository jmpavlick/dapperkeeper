﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoaltenderWeb.Data.Repositories;
using GoaltenderWeb.Data.Models;

namespace GoaltenderWeb.Data.Tests
{
	static class SetupTestData
	{
		public static void ClassInitialize()
		{
			SqlTests st = new SqlTests();
			st.Execute_CreateTables_SQLScript();
		}

		public static void TestInitialize()
		{
			BaseRepository<string> b = new BaseRepository<string>();
			b.ExecuteStatement(@"
				INSERT INTO dbo.LDAPUsers(UserLoginName, UserDisplayName, ManagerLoginName) VALUES ('EVANSDIST\jaevans', 'John A Evans', NULL);
				INSERT INTO dbo.LDAPUsers(UserLoginName, UserDisplayName, ManagerLoginName) VALUES ('EVANSDIST\cvannoord', 'Craig Van Noord', 'EVANSDIST\jaevans');
				INSERT INTO dbo.LDAPUsers(UserLoginName, UserDisplayName, ManagerLoginName) VALUES ('EVANSDIST\jpavlick', 'John Pavlick', 'EVANSDIST\cvannoord');
				INSERT INTO dbo.LDAPUsers(UserLoginName, UserDisplayName, ManagerLoginName) VALUES ('EVANSDIST\atrocki', 'Anthony Trocki', 'EVANSDIST\jaevans');
				INSERT INTO dbo.GoalActions (StrategicBusinessUnitName, Quarter, Year, Goal, Objective, ActionStep, ActualOutcome, StatusColor, UserLoginName, DateCreated, DateModified) VALUES ('Alden', '1', '2017', 'Decrease Expense', 'Lorem Ipsum', 'Dolor sit amet', NULL, 'Green', 'EVANSDIST\jpavlick', GETDATE(), GETDATE());
			");

			int goalActionId = b.SelectScalar<int>(@"SELECT MAX(GoalActionId) FROM dbo.GoalActions WHERE UserLoginName = 'EVANSDIST\jpavlick'");

			b.ExecuteStatement(string.Format(@"
				INSERT INTO dbo.ProgressComments (GoalActionId, ProgressCommentText, UserLoginName) VALUES ({0}, N'This is a test progress comment', 'EVANSDIST\jaevans');
				INSERT INTO dbo.ProgressComments (GoalActionId, ProgressCommentText, UserLoginName) VALUES ({0}, N'This is another test progress comment', 'EVANSDIST\jaevans');
				INSERT INTO dbo.ProgressComments (GoalActionId, ProgressCommentText, UserLoginName) VALUES ({0}, N'This is another test progress comment', 'EVANSDIST\jaevans');
			", goalActionId));
		}

		public static void TestCleanup()
		{
			BaseRepository<string> b = new BaseRepository<string>();
			b.ExecuteStatement("DELETE FROM dbo.ProgressComments");
			b.ExecuteStatement("DELETE FROM dbo.GoalActions");
			b.ExecuteStatement("DELETE FROM dbo.LDAPUsers");
		}
	}
}
