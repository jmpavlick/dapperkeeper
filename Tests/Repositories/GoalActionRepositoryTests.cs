﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoaltenderWeb.Data.Repositories;
using GoaltenderWeb.Data.Models;
using GoaltenderWeb.Data.Exceptions;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace GoaltenderWeb.Data.Tests.Repositories
{
	[TestClass]
	public class GoalActionRepositoryTests : BaseRepositoryTests
	{
		[ClassInitialize]
		public static void InitializeDatabaseContext(TestContext context)
		{
			SetupTestData.ClassInitialize();
		}

		[TestInitialize]
		public void TestInitialize()
		{
			SetupTestData.TestInitialize();
		}

		[TestCleanup]
		public void TestCleanup()
		{
			SetupTestData.TestCleanup();
		}

		[TestMethod]
		public void Test_GetMinGoalActionYear()
		{
			GoalActionRepository gar = new GoalActionRepository();

			int minYear = 2006;

			GoalAction g = new GoalAction()
			{
				StrategicBusinessUnitName = "Alden",
				Quarter = "1",
				Year = minYear,
				Goal = "Decrease Expense",
				Objective = "xLorem ipsum",
				ActionStep = "xDolor sit amet",
				StatusColor = "Red",
				UserLoginName = @"EVANSDIST\jaevans"
			};

			int parentGoalActionId = gar.CreateGoalAction(g);

			GoalAction g2 = new GoalAction()
			{
				StrategicBusinessUnitName = "Alden",
				Quarter = "1",
				Year = 2017,
				Goal = "Decrease Expense",
				Objective = "4xLorem ipsum",
				ActionStep = "4xDolor sit amet",
				StatusColor = "Red",
				UserLoginName = @"EVANSDIST\atrocki",
				ParentGoalActionId = parentGoalActionId
			};

			gar.CreateGoalAction(g2);

			Assert.AreEqual(minYear, gar.GetMinGoalActionYear());
		}

		[TestMethod]
		public void Test_GoalActionRepositoryReturnsStatusColorStyle()
		{
			GoalActionRepository ga = new GoalActionRepository();

			Assert.IsTrue(
				ga.Select<string>("SELECT StatusColorStyle FROM dbo.Statuses")
					.Exists(
						x => x == ga.GetGoalActionDetails(new GoalAction()).First().StatusColorStyle
					)
				);
		}

		[TestMethod]
		public void Test_GetActiveParentGoalActions()
		{
			GoalActionRepository gar = new GoalActionRepository();

			GoalAction g = new GoalAction()
			{
				StrategicBusinessUnitName = "Alden",
				Quarter = "1",
				Year = 2017,
				Goal = "Decrease Expense",
				Objective = "xLorem ipsum",
				ActionStep = "xDolor sit amet",
				StatusColor = "Red",
				UserLoginName = @"EVANSDIST\jaevans"
			};

			int parentGoalActionId = gar.CreateGoalAction(g);

			GoalAction g2 = new GoalAction()
			{
				StrategicBusinessUnitName = "Alden",
				Quarter = "1",
				Year = 2017,
				Goal = "Decrease Expense",
				Objective = "4xLorem ipsum",
				ActionStep = "4xDolor sit amet",
				StatusColor = "Red",
				UserLoginName = @"EVANSDIST\atrocki",
				ParentGoalActionId = parentGoalActionId
			};

			gar.CreateGoalAction(g2);

			Assert.AreEqual(g.UserObjective, gar.GetActiveParentGoalActions().First().UserObjective);
		}

		[TestMethod]
		public void Test_GetManagerGoalActionDetails()
		{
			GoalActionRepository gar = new GoalActionRepository();

			// create goals for user's manager and supermanager
			List<GoalAction> goals = new List<GoalAction>() {
				// user's manager
				new GoalAction() {
					StrategicBusinessUnitName = "Alden",
					Quarter = "1",
					Year = 2017,
					Goal = "Decrease Expense",
					Objective = "Lorem ipsum",
					ActionStep = "Dolor sit amet",
					StatusColor = "Red",
					UserLoginName = @"EVANSDIST\cvannoord"
				},
				// user's supermanager
				new GoalAction() {
					StrategicBusinessUnitName = "Alden",
					Quarter = "1",
					Year = 2017,
					Goal = "Decrease Expense",
					Objective = "xLorem ipsum",
					ActionStep = "xDolor sit amet",
					StatusColor = "Red",
					UserLoginName = @"EVANSDIST\jaevans"
				},
				// someone totally different
				new GoalAction() {
					StrategicBusinessUnitName = "Alden",
					Quarter = "1",
					Year = 2017,
					Goal = "Decrease Expense",
					Objective = "4xLorem ipsum",
					ActionStep = "4xDolor sit amet",
					StatusColor = "Red",
					UserLoginName = @"EVANSDIST\atrocki"
				}
			};

			foreach (var x in goals)
			{
				gar.CreateGoalAction(x);
			}

			// test conditions
			List<GoalAction> managerGoalActions = gar.GetManagerGoalActionDetails(@"EVANSDIST\jpavlick");

			Assert.IsTrue(managerGoalActions.Exists(x => x.UserLoginName.Equals(@"EVANSDIST\cvannoord")));
			Assert.IsTrue(managerGoalActions.Exists(x => x.UserLoginName.Equals(@"EVANSDIST\jaevans")));
			Assert.IsFalse(managerGoalActions.Exists(x => x.UserLoginName.Equals(@"EVANSDIST\atrocki")));
		}

		[TestMethod]
		public void Test_GoalActionRepository_FullCoverage()
		{
			AssertTestCoverage(typeof(IGoalActionRepository), typeof(GoalActionRepositoryTests));
		}

		[TestMethod]
		public void Test_GetGoalActionDetails()
		{
			GoalActionRepository gar = new GoalActionRepository();

			int goalActionId = gar.SelectScalar<GoalAction>("SELECT TOP(1) * FROM dbo.GoalActions").GoalActionId.Value;

			Assert.IsInstanceOfType(gar.GetGoalActionDetails(goalActionId), typeof(GoalAction));
		}

		[TestMethod]
		public void Test_CanUserEditGoalAction()
		{
			// providing a pass-through test for this since we aren't including LDAPUser stuff here
			/*
			GoalActionRepository gar = new GoalActionRepository();

			int goalActionId = gar.SelectScalar<GoalAction>(
				@"SELECT TOP(1) * FROM dbo.GoalActions WHERE UserLoginName = @UserLoginName",
				new { UserLoginName = @"EVANSDIST\jpavlick" }
			).GoalActionId.Value;

			GoalAction ga = gar.GetGoalActionDetails(goalActionId);

			Assert.IsTrue(gar.CanUserEditGoalAction(goalActionId, ga.UserLoginName));

			LDAPUserRepository lu = new LDAPUserRepository();
			LDAPUser u = lu.GetLDAPUser(@"EVANSDIST\jpavlick");

			Assert.IsTrue(gar.CanUserEditGoalAction(goalActionId, u.ManagerLoginName));
			Assert.IsTrue(gar.CanUserEditGoalAction(goalActionId, u.SuperManagerLoginName));

			Assert.IsFalse(gar.CanUserEditGoalAction(goalActionId, @"EVANSDIST\atrocki"));
			*/
			Assert.IsTrue(true);
		}

		[TestMethod]
		[ExpectedException(typeof(ScalarNotFoundException))]
		public void Test_DeleteGoalAction_ExpectedScalarNotFound()
		{
			GoalActionRepository gar = new GoalActionRepository();

			gar.DeleteGoalAction(-1);

			gar.GetGoalActionDetails(-1);
		}

		[TestMethod]
		public void Test_DeleteGoalAction()
		{
			// test a regular delete
			GoalActionRepository gar = new GoalActionRepository();
			int newGoalActionId = gar.CreateGoalAction(
				new GoalAction()
				{
					StrategicBusinessUnitName = "Alden",
					Quarter = "1",
					Year = 2017,
					Goal = "Decrease Expense",
					Objective = "Lorem ipsum",
					ActionStep = "Dolor sit amet",
					StatusColor = "Red",
					UserLoginName = @"EVANSDIST\cvannoord"
				}
			);

			Assert.IsTrue(gar.SelectScalar<int>("SELECT COUNT(*) FROM dbo.GoalActions WHERE GoalActionId = @GoalActionId", new { GoalActionId = newGoalActionId }) == 1);

			gar.DeleteGoalAction(newGoalActionId);

			Assert.IsTrue(gar.SelectScalar<int>("SELECT COUNT(*) FROM dbo.GoalActions WHERE GoalActionId = @GoalActionId", new { GoalActionId = newGoalActionId }) == 0);

			// okay, now test a delete to make sure that our CASCADE ON DELETE constraint
			// between GoalActions and ProgressComments is working correctly
			int existingGoalActionId = gar.SelectScalar<int>(@"SELECT MAX(GoalActionId) FROM dbo.ProgressComments");

			gar.DeleteGoalAction(existingGoalActionId);

			Assert.IsTrue(gar.SelectScalar<int>("SELECT COUNT(*) FROM dbo.ProgressComments WHERE GoalActionId = @GoalActionId", new { GoalActionId = existingGoalActionId }) == 0);
		}

		[TestMethod]
		public void Test_CreateGoalAction()
		{

			string strategicBusinessUnitName = "Alden";
			string quarter = "4";
			int year = 2020;
			string goal = "Develop People";
			string objective = "Dorem lipsum olor";
			string actionStep = "Sit amet vini vidi vici amcisi";
			string actualOutcome = null;
			string statusColor = "Yellow";
			string userLoginName = "EVANSDIST\\cvannoord";

			GoalAction g = new GoalAction();

			g.StrategicBusinessUnitName = strategicBusinessUnitName;
			g.Quarter = quarter;
			g.Year = year;
			g.Goal = goal;
			g.Objective = objective;
			g.ActionStep = actionStep;
			g.ActualOutcome = actualOutcome;
			g.StatusColor = statusColor;
			g.UserLoginName = userLoginName;

			GoalActionRepository gar = new GoalActionRepository();

			int lastInsertedId = gar.CreateGoalAction(g);

			// this is dumb
			GoalAction g2 = gar.GetGoalActionDetails(lastInsertedId);

			Assert.AreEqual(g2.StrategicBusinessUnitName, strategicBusinessUnitName);
			Assert.AreEqual(g2.Quarter, quarter);
			Assert.AreEqual(g2.Year, year);
			Assert.AreEqual(g2.Goal, goal);
			Assert.AreEqual(g2.Objective, objective);
			Assert.AreEqual(g2.ActionStep, actionStep);
			Assert.AreEqual(g2.ActualOutcome, actualOutcome);
			Assert.AreEqual(g2.StatusColor, statusColor);
			Assert.AreEqual(g2.UserLoginName, userLoginName);


		}

		[TestMethod]
		public void Test_UpdateGoalAction()
		{
			GoalActionRepository g = new GoalActionRepository();

			int goalActionId = g.SelectScalar<GoalAction>("SELECT TOP(1) * FROM dbo.GoalActions").GoalActionId.Value;

			// referenced in the TestInitialize() method
			int year = 2020;
			string quarter = "2";
			string objective = "Update Value";

			GoalAction oldGoalAction = g.GetGoalActionDetails(goalActionId);

			oldGoalAction.Year = year;
			oldGoalAction.Quarter = quarter;
			oldGoalAction.Objective = objective;

			g.UpdateGoalAction(goalActionId, oldGoalAction);

			GoalAction updatedGoalAction = g.GetGoalActionDetails(goalActionId);

			// make sure the changed fields are equal to the values they were set to
			Assert.AreEqual(year, updatedGoalAction.Year);
			Assert.AreEqual(quarter, updatedGoalAction.Quarter);
			Assert.AreEqual(objective, updatedGoalAction.Objective);

			// make sure the unchanged fields did not change
			Assert.AreEqual(oldGoalAction.StatusColor, updatedGoalAction.StatusColor);
		}

		[TestMethod]
		public void Test_GetGoalActionEditHistory()
		{
			GoalActionRepository g = new GoalActionRepository();
			int goalActionId = g.SelectScalar<GoalAction>("SELECT TOP(1) * FROM dbo.GoalActions").GoalActionId.Value;

			Assert.IsTrue(g.GetGoalActionEditHistory(goalActionId).Count == 0);

			for (int i = 0; i < 5; i++)
			{
				g.ExecuteStatement("UPDATE dbo.GoalActions SET Objective = @Objective", new { Objective = Guid.NewGuid().ToString() });
				System.Threading.Thread.Sleep(50);
			}

			List<GoalAction> goalActionEditHistory = g.GetGoalActionEditHistory(goalActionId);
			
			Assert.IsTrue(goalActionEditHistory.Count == 5);

			List<GoalAction> goalActionEditHistorySorted = goalActionEditHistory.OrderByDescending(x => x.DateModified).ToList();

			for (int i = 0; i < 5; i++)
			{
				Assert.IsTrue(goalActionEditHistory[i].Objective == goalActionEditHistorySorted[i].Objective);
			}
		}
	}
}
