﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoaltenderWeb.Data.Repositories;
using GoaltenderWeb.Data.Models;
using System.Collections.Generic;

namespace GoaltenderWeb.Data.Tests.Repositories
{
	[TestClass]
	public class BaseRepositoryTests
	{
		[ClassInitialize]
		public static void InitializeDatabaseContext(TestContext context)
		{
			SetupTestData.ClassInitialize();
		}

		public void AssertTestCoverage(Type repositoryInterfaceType, Type testType)
		{
			foreach (MethodInfo method in repositoryInterfaceType.GetMethods())
			{
				if (testType.GetMethods().Where(x => x.Name == string.Format("Test_{0}", method.Name)).Count() == 0)
				{
					throw new Exception(string.Format("AssertTestCoverage: Type {0}, Method {1}: Missing test coverage", repositoryInterfaceType.Name, method.Name));
				}
			}
		}

		[TestMethod]
		public void Test_CreateParametersList()
		{
			BaseRepository<GoalAction> b = new BaseRepository<GoalAction>();

			// make sure the InsertIgnore filtering works!
			List<string> s = b.CreateParametersList(new GoalAction(), false);
			Assert.IsTrue(s.Contains("MasterStrategicBusinessUnitName = @MasterStrategicBusinessUnitName") == false && s.Count > 0);

			List<string> s2 = b.CreateParametersList(new GoalAction() { StatusColor = "Red" }, true);
			Assert.IsTrue(s2.Contains("StatusColor = @StatusColor") && s2.Count == 1);

			// make sure that we're filtering out the BaseModel-inherited properties
			Assert.IsTrue(s.Concat(s2).Where(x => x.Contains("ActualTableName")).Count() == 0);

			// test properties that have default values, to make sure that they're added if they're not null
			List<string> s3 = b.CreateParametersList(new GoalAction() { DateCreated = DateTime.Now.AddDays(-2.0) }, true);
			Assert.IsTrue(s3.Contains("DateCreated = @DateCreated"));
			Assert.IsFalse(s3.Contains("DateModified = @DateModified"));

			List<string> s4 = b.CreateParametersList(new GoalAction() { DateCreated = DateTime.Now.AddDays(-2.0) }, false);
			Assert.IsTrue(s4.Contains("DateCreated = @DateCreated"));
			Assert.IsFalse(s4.Contains("DateModified = @DateModified"));

			// what about properties labeled "Key"?
			List<string> s5 = b.CreateParametersList(new GoalAction() { GoalActionId = 666 });
			Assert.IsTrue(s5.Contains("GoalActionId = @GoalActionId"));
		}
	}
}
