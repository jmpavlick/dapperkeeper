﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GoaltenderWeb.Data.Models;
using System.Reflection;
using System.Linq;

namespace GoaltenderWeb.Data.Tests.Models
{
	[TestClass]
	public class GoalActionTests
	{
		// add this in to BaseModel somehow? as an extension for typeof(BaseModel).GetProperties, maybe like typeof(GoalAction).GetNonBaseProperties()?
		public Func<PropertyInfo, bool> FilterBaseProperties = x => typeof(BaseModel).GetProperties().Select(y => y.Name).Contains(x.Name) == false;

		[TestMethod]
		public void Test_GoalActionUserObjective()
		{
			string userLoginName = @"EVANSDIST\jpavlick";
			string objective = "Lorem ipsum dolor sit amet";

			GoalAction ga = new GoalAction()
			{
				UserLoginName = userLoginName,
				Objective = objective
			};

			Assert.AreEqual(ga.UserObjective, string.Format("{0} - {1}", userLoginName, objective));
		}

		[TestMethod]
		public void Test_GoalActionHasStatusColorStyle()
		{
			Assert.IsTrue(
				typeof(GoalAction).GetNonBaseModelProperties().ToList().Exists(x => x.Name == "StatusColorStyle")
			);
		}

		// commenting this out because I'm not including the actual Goaltender web stuff with this release
		// but it was helpful at the time and I'd like some record of how I did it
		/*
		[TestMethod]
		public void Test_GoalActionToDataTableLinter()
		{
			// we need to deprecate the .ToDataTable() function call
			// where we're going, we don't need (much) reflection
			// "C:\\Users\\jpavlick\\src\\goaltender\\GoaltenderWeb\\GoaltenderWeb.Data.Tests\\bin\\Debug\\GoaltenderWeb.Data.Tests.dll"
			string location = Assembly.GetExecutingAssembly().Location;
			string[] classFileText = File.ReadAllLines(Path.Combine(location, @"..\..\..\..\GoaltenderWeb\Controllers\GoalController.cs"));

			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < classFileText.Length; i++)
			{
				if (classFileText[i].Contains(".ToDataTable()"))
				{
					sb.AppendFormat("{0}: {1}{2}", i + 1, classFileText[i], Environment.NewLine);
				}
			}

			if (sb.Length > 0)
			{
				Assert.Fail(sb.ToString());
			}
		}
		*/

		// commenting this out becuase we aren't including web stuff in this release
		/*
		[TestMethod]
		public void Test_GoalAction_FormCollectionToGoalAction()
		{
			GoalAction ga = new GoalAction() {
				GoalActionId = 666,
				MasterStrategicBusinessUnitName = "Lorem",
				StrategicBusinessUnitName = "ipsum",
				Quarter = "3",
				Year = 2020,
				Goal = "dolor",
				Objective = "sit",
				ActionStep = "amet",
				ActualOutcome = "consectetur",
				StatusColor = "adipiscing",
				UserLoginName = "elit",
				DateCreated = DateTime.UtcNow,
				DateModified = DateTime.Now,
				ParentGoalActionId = 999,
				LastEditedUserLoginName = "sed",
				StatusColorStyle = "dolor"
			};

			// this test is useless if we aren't making sure that our GoalAction
			// isn't NULL to begin with
			Assert.IsFalse(_HasNullProperties(ga));

			FormCollection fc = new FormCollection();

			foreach (var property in typeof(GoalAction).GetProperties().Where(FilterBaseProperties))
			{
				fc.Add(property.Name, property.GetValue(ga).ToString());
			}

			GoalAction fcga = fc.ToGoalAction();

			Assert.IsFalse(_HasNullProperties(fcga), "fcga has null properties");

			Assert.IsFalse(
				typeof(GoalAction).GetProperties().Where(FilterBaseProperties).Select(
					x => x.GetValue(ga).ToString().Equals(x.GetValue(fcga).ToString())).Contains(false),
				"fcga does not equal ga"
			);
		}
		*/

		/*
			@-\-x--/--  @-\-x--/---  @-\-x--/---
				Roses are red
				Violets are best
				You're testing your code,
				But are you testing your TESTS?
			---/--x-\-@  ---/--x-\-@  ---/--x-\-@
		*/
		[TestMethod]
		public void Test_GoalActionTests_HasNullPropertiesHelperMethod()
		{
			Assert.IsTrue(_HasNullProperties(new { s = (string)null, q = "hello world", x = 4}));
			Assert.IsFalse(_HasNullProperties(new { s = "lorem ipsum", q = "hello world", x = 4 }));
		}

		private static bool _HasNullProperties(object o)
		{
			// ignore BaseModel properties
			return o.GetType()
				.GetProperties()
				.Where(x => typeof(BaseModel).GetProperties().Select(y => y.Name).Contains(x.Name) == false)
				.Where(x => x.GetValue(o) == null)
				.Count() > 0;
		}
	}
}
