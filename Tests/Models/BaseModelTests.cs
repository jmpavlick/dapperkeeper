﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using GoaltenderWeb.Data.Models;
using System.Linq;

namespace GoaltenderWeb.Data.Tests.Models
{
	[TestClass]
	public class BaseModelTests
	{
		[TestMethod]
		public void Test_BaseModelContainsGetNonBaseModelPropertiesTypeExtension()
		{
			Func<PropertyInfo, bool> FilterBaseModelProperties = x => typeof(BaseModel).GetProperties().Select(y => y.Name).Contains(x.Name) == false;

			PropertyInfo[] usingExtension = typeof(GoalAction).GetNonBaseModelProperties().OrderBy(q => q.Name).ToArray();
			PropertyInfo[] notUsingExtension = typeof(GoalAction).GetProperties().Where(FilterBaseModelProperties).OrderBy(q => q.Name).ToArray();

			Assert.AreEqual(usingExtension.Length, notUsingExtension.Length);

			for (int i = 0; i < usingExtension.Length; i++)
			{
				Assert.AreEqual(usingExtension[i].Name, notUsingExtension[i].Name);
			}
		}
	}
}
