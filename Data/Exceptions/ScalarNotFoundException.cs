﻿using System;

namespace GoaltenderWeb.Data.Exceptions
{
	public class ScalarNotFoundException : Exception
	{
		public ScalarNotFoundException() { }

		public ScalarNotFoundException(string message) { }

		public ScalarNotFoundException(string message, Exception innerException) { }
	}
}
