﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoaltenderWeb.Data
{
	public sealed class DatabaseContext
	{
		private static string _connectionString;

		public static string ConnectionString
		{
			get
			{
				if (_connectionString == null)
				{
					throw new Exception("ConnectionString not set");
				}

				return _connectionString;
			}
			set
			{
				if (_connectionString != null && _connectionString.Equals(value) == false)
				{
					throw new Exception("ConnectionString can only be set once");
				}

				_connectionString = value;
			}
		}
	}
}
