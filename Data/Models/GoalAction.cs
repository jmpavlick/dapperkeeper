﻿using System;
using GoaltenderWeb.Data.Attributes;

namespace GoaltenderWeb.Data.Models
{
	public class GoalAction : BaseModel
	{
		public GoalAction()
		{
			ActualTableName = "GoalActions";
		}

		[Key]
		public int? GoalActionId { get; set; }
		[InsertIgnore]
		public string MasterStrategicBusinessUnitName { get; set; }
		public string StrategicBusinessUnitName { get; set; }
		public string Quarter { get; set; }
		public int? Year { get; set; }
		public string Goal { get; set; }
		public string Objective { get; set; }
		public string ActionStep { get; set; }
		public string ActualOutcome { get; set; }
		public string StatusColor { get; set; }
		public string UserLoginName { get; set; }
		[Default]
		public DateTime? DateCreated { get; set; }
		[Default]
		public DateTime? DateModified { get; set; }
		public int? ParentGoalActionId { get; set; }
		[InsertIgnore]
		public string UserObjective { get { return string.Format("{0} - {1}", UserLoginName, Objective); } }
		public string LastEditedUserLoginName { get; set; }
		[InsertIgnore]
		public string StatusColorStyle { get; set; }
	}
}
