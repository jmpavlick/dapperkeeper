﻿using System.Reflection;
using System.Linq;
using System;

namespace GoaltenderWeb.Data.Models
{
	public static class BaseModelTypeExtension
	{
		public static PropertyInfo[] GetNonBaseModelProperties(this Type t)
		{
			return t.GetProperties().Where(x => typeof(BaseModel).GetProperties().Select(y => y.Name).Contains(x.Name) == false).ToArray();
		}
	}
}
