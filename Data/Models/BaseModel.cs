﻿namespace GoaltenderWeb.Data.Models
{
	public class BaseModel
	{
		public string DatabaseName { get; protected set; }
		public string DatabaseSchemaName { get; protected set; }
		public string ActualTableName { get; protected set; }

		public BaseModel()
		{
			// defaults
			DatabaseSchemaName = "dbo";
			ActualTableName = this.GetType().Name;
			DatabaseName = null;
		}
	}
}
