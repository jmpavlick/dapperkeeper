﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using GoaltenderWeb.Data.Models;
using GoaltenderWeb.Data.Exceptions;

namespace GoaltenderWeb.Data.Repositories
{
	public class GoalActionRepository : BaseRepository<GoalAction>, IGoalActionRepository
	{
		public List<GoalAction> GetManagerGoalActionDetails(string userLoginName)
		{
			return Select(
				@";WITH Management AS (
					SELECT
						u.ManagerLoginName
					FROM dbo.V_LDAPUsers AS u
					WHERE u.UserLoginName = @UserLoginName
					UNION ALL
					SELECT
						u.SuperManagerLoginName
					FROM dbo.V_LDAPUsers AS u
					WHERE u.UserLoginName = @UserLoginName
				)
				SELECT
					*
				FROM dbo.V_GoalActions AS g
				WHERE g.UserLoginName IN (
					SELECT
						*
					FROM Management
				)",
				new { UserLoginName = userLoginName }
			);
		}

		public List<GoalAction> GetGoalActionDetails(GoalAction goalActionFilter, string containsText = null)
		{
			// use a GoalAction object's property values as filters for the WHERE clause
			string selectFormat = "SELECT * FROM dbo.V_GoalActions WHERE 1 = 1 {0} ORDER BY Year, Quarter";
			StringBuilder conditionalClause = new StringBuilder();

			Type type = typeof(GoalAction);
			PropertyInfo[] properties = type.GetProperties();

			string parametersList = string.Join(" AND ", CreateParametersList(goalActionFilter, true));

			if (String.IsNullOrWhiteSpace(parametersList) == false)
			{
				conditionalClause.AppendFormat(
					" AND {0}",
					parametersList
				);
			}

			if (string.IsNullOrWhiteSpace(containsText) == false)
			{
				conditionalClause.AppendFormat(
					" AND ({0})",
					string.Join(" OR ", containsText.Split(' ').Select(
						x => "CONCAT(Objective, ' ', ActionStep, ' ', ActualOutcome) LIKE '%" + x + "%'"
					))
				);
			}

			return Select(string.Format(selectFormat, conditionalClause.ToString()), goalActionFilter);
		}

		public GoalAction GetGoalActionDetails(int goalActionId)
		{
			// to maintain consistency with BaseRepository.SelectScalar<>(), throw a ScalarNotFoundException
			try
			{
				return GetGoalActionDetails(new GoalAction() { GoalActionId = goalActionId }).First();
			}
			catch (InvalidOperationException ex)
			{
				throw new ScalarNotFoundException(ex.Message);
			}
		}

		public bool CanUserEditGoalAction(int goalActionId, string userLoginName)
		{
			// this is the original implementation of the method
			// however, to keep things simple, I'm providing an implementation
			// that isn't dependent on LDAPUserRepository
			// (the original implementation allows a user's manager and manager's manager to edit their goals)

			/*
			string owner = GetGoalActionDetails(goalActionId).UserLoginName;
			LDAPUserRepository lu = new LDAPUserRepository();
			LDAPUser u = lu.GetLDAPUser(owner);

			List<string> usersThatCanEdit = new List<string>() {
				u.UserLoginName,
				u.ManagerLoginName,
				u.SuperManagerLoginName
			};

			return usersThatCanEdit.Contains(userLoginName);
			*/

			// with this implementation, only a user can edit his goals
			return GetGoalActionDetails(goalActionId).UserLoginName.Equals(userLoginName);
		}

		public void DeleteGoalAction(int goalActionId)
		{
			ExecuteStatement("DELETE FROM dbo.GoalActions WHERE GoalActionId = @GoalActionId", new { GoalActionId = goalActionId });
		}

		public List<GoalAction> GetGoalActionDetails(string userLoginName)
		{
			return GetGoalActionDetails(new GoalAction() { UserLoginName = userLoginName });
		}

		public int CreateGoalAction(GoalAction goalAction)
		{
			InsertScalar(goalAction);
			// TODO: refactor, this isn't necessarily always going to work - what if there's a race condition?
			return SelectScalar<int>("SELECT CONVERT(INTEGER, IDENT_CURRENT('dbo.GoalActions'))");
		}

		public int UpdateGoalAction(int goalActionId, GoalAction goalAction)
		{
			goalAction.GoalActionId = goalActionId;
			ExecuteStatement(
				string.Format(
					"UPDATE dbo.GoalActions SET {0} WHERE GoalActionId = @GoalActionId",
					string.Join(", ", CreateParametersList(goalAction, false).Where(x => x != "GoalActionId = @GoalActionId"))
				),
				goalAction
			);

			return goalActionId;
		}

		public List<GoalAction> GetActiveParentGoalActions()
		{
			/*
				this exists for reporting purposes - we need to be able to see the user and objective
				for goals that are "parent" goals to other goals
			*/

			return Select(
				@"SELECT
					*
				FROM dbo.V_GoalActions
				WHERE GoalActionId IN(
					SELECT DISTINCT
					ParentGoalActionId
				FROM dbo.GoalActions
				)
				ORDER BY UserLoginName, Objective"
			);
		}

		public List<GoalAction> GetGoalActionEditHistory(int goalActionId)
		{
			return Select("SELECT * FROM dbo.V_GoalActionsEditHistory WHERE GoalActionId = @GoalActionId ORDER BY DateModified DESC", new { GoalActionId = goalActionId });
		}

		public int GetMinGoalActionYear()
		{
			return SelectScalar<int>(@"
				;WITH MinGoalActionYearCandidates AS (
					SELECT
						MIN(Year) AS MinGoalActionYear
					FROM dbo.GoalActions AS ga
					UNION ALL
					SELECT
						DATEPART(YEAR, GETDATE()) AS MinGoalActionYear
				)
				SELECT
					MIN(MinGoalActionYear) AS MinGoalActionYear
				FROM MinGoalActionYearCandidates
			");
		}

	}

	public interface IGoalActionRepository
	{
		List<GoalAction> GetManagerGoalActionDetails(string userLoginName);
		GoalAction GetGoalActionDetails(int goalActionId);
		bool CanUserEditGoalAction(int goalActionId, string userLoginName);
		void DeleteGoalAction(int goalActionId);
		List<GoalAction> GetGoalActionDetails(string userLoginName);
		List<GoalAction> GetGoalActionDetails(GoalAction goalActionFilter, string containsText);
		int CreateGoalAction(GoalAction goalAction);
		int UpdateGoalAction(int goalActionId, GoalAction goalAction);
		List<GoalAction> GetActiveParentGoalActions();
		List<GoalAction> GetGoalActionEditHistory(int goalActionId);
	}
}
