﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Data.SqlClient;
using Dapper;
using GoaltenderWeb.Data.Attributes;
using GoaltenderWeb.Data.Models;
using GoaltenderWeb.Data.Exceptions;

namespace GoaltenderWeb.Data.Repositories
{
	public class BaseRepository<T> : IBaseRepository<T>
	{
		private string _connectionString { get; set; }

		public BaseRepository(string connectionString = null)
		{
			_connectionString = string.IsNullOrEmpty(connectionString) ? DatabaseContext.ConnectionString : connectionString;
		}

		// create parameters for Dapper's parameters object to hook into
		public List<string> CreateParametersList(T obj, bool filterNulls = true)
		{
			return CreateParametersList<T>(obj, filterNulls);
		}

		public List<string> CreateParametersList<T2>(T2 obj, bool filterNulls = true)
		{
			Type type = typeof(T2);
			PropertyInfo[] properties = type.GetNonBaseModelProperties();

			return properties
				.Where(x => x.CustomAttributes.Where(y => y.AttributeType == typeof(InsertIgnore)).Count() == 0)
				// if it's NOT a default - or, if it is, it has a value
				.Where(x => (x.CustomAttributes.Select(y => y.AttributeType == typeof(DefaultAttribute)).Count() == 0)
					|| ((x.CustomAttributes.Select(y => y.AttributeType == typeof(DefaultAttribute)).Count() == 1)
						&& type.GetProperty(x.Name).GetValue(obj) != null))
				.Where(x => (type.GetProperty(x.Name).GetValue(obj) != null) || filterNulls == false)
				.Select(x => string.Format("{0} = @{0}", x.Name))
				.ToList();
		}

		public void ExecuteStatement(string query, object parameters = null)
		{
			using (SqlConnection conn = new SqlConnection(_connectionString))
			{
				conn.Open();
				conn.Execute(query, parameters);
				conn.Close();
			}
		}

		// T2 is any type; T is the BaseRepository's implementation type
		public List<T2> Select<T2>(string query, object parameters = null)
		{
			List<T2> resultSet = new List<T2>();

			using (SqlConnection conn = new SqlConnection(_connectionString))
			{
				conn.Open();
				resultSet = conn.Query<T2>(query, parameters).ToList();
				conn.Close();
			}

			return resultSet;
		}

		public List<T> Select(string query, object parameters = null)
		{
			return Select<T>(query, parameters);
		}

		public T2 SelectScalar<T2>(string query, object parameters = null)
		{
			try
			{
				return Select<T2>(query, parameters).First();
			}
			catch (InvalidOperationException ex)
			{
				throw new ScalarNotFoundException(string.Format("No result set found for query: {0}", query));
			}
		}

		public T SelectScalar(string query, object parameters = null)
		{
			return SelectScalar<T>(query, parameters);
		}

		public void Insert<T2>(List<T2> insertData, bool overrideKeys = false, bool overrideDefaults = false)
		{
			string insertFormat = "INSERT INTO {0}.[{1}] ( {2} ) VALUES ({3}) ;";

			// build insert column list
			Type type = typeof(T2);

			List<string> columnList = new List<string>();

			// any properties declared in BaseModel are metadata, not table columns
			// this can be refactored more, into one line
			List<PropertyInfo> columnProperties = type.GetNonBaseModelProperties().ToList();

			columnList = columnProperties.Where(x => x.CustomAttributes.Select(y => y.AttributeType == typeof(InsertIgnore)).Count() == 0)
				.Where(x => (x.CustomAttributes.Select(y => y.AttributeType == typeof(DefaultAttribute)).Count() == 0) || (overrideDefaults == true))
				.Where(x => (x.CustomAttributes.Select(y => y.AttributeType == typeof(KeyAttribute)).Count() == 0) || (overrideKeys == true))
				.Select(x => x.Name).ToList();

			string columns = string.Join(", ", columnList);
			string values = string.Join(", ", columnList.Select(x => string.Format("@{0}", x)));

			foreach (var x in insertData)
			{
				string databaseSchemaName = (string)type.GetProperty("DatabaseSchemaName").GetValue(x);
				string tableName = (string)type.GetProperty("ActualTableName").GetValue(x);
				string overriddenDatabaseName = (string)type.GetProperty("DatabaseName").GetValue(x);

				databaseSchemaName = overriddenDatabaseName == null ? databaseSchemaName : string.Format("{0}.{1}", overriddenDatabaseName, databaseSchemaName);

				ExecuteStatement(string.Format(insertFormat, databaseSchemaName, tableName, columns, values), x);
			}
		}

		public void Insert(List<T> insertData, bool overrideKeys = false, bool overrideDefaults = false)
		{
			Insert<T>(insertData, overrideKeys, overrideDefaults);
		}

		public void InsertScalar<T2>(T2 insertData, bool overrideKeys = false, bool overrideDefaults = false)
		{
			Insert(new List<T2> { insertData }, overrideKeys, overrideDefaults);
		}

		public void InsertScalar(T insertData, bool overrideKeys = false, bool overrideDefaults = false)
		{
			InsertScalar<T>(insertData, overrideKeys, overrideDefaults);
		}
	}

	public interface IBaseRepository<T>
	{
		List<T> Select(string query, object parameters = null);
		T SelectScalar(string query, object parameters = null);
		void Insert(List<T> insertData, bool overrideKeys = false, bool overrideDefaults = false);
		void InsertScalar(T insertData, bool overrideKeys = false, bool overrideDefaults = false);
		void ExecuteStatement(string query, object parameters = null);
	}
}
