USE Goaltender
GO

SET NOEXEC OFF

-- let's not ever footgun ourselves in Prod ever again
IF (@@SERVERNAME = 'EVANS-SQLPW01')
BEGIN
	BEGIN TRY
		DECLARE @BackupName SYSNAME = CONCAT('Goaltender-Full-', REPLACE(REPLACE(CONVERT(NVARCHAR(32), GETDATE(), 121), ' ', '_'), ':', ''), '.bak')
		DECLARE @BackupFilename SYSNAME = CONCAT(N'R:\SQL_DATA\MSSQL11.MSSQLSERVER\MSSQL\Backup', CHAR(92), @BackupName)

		BACKUP DATABASE [Goaltender]
			TO  DISK = @BackupFilename
			WITH NOFORMAT, NOINIT,
			NAME = @BackupName, SKIP, NOREWIND, NOUNLOAD,  STATS = 10
	END TRY
	BEGIN CATCH
		-- bleed everywhere if the backup failed in Production
		PRINT 'Backup failed'
		SET NOEXEC ON
	END CATCH
END
GO

-- rewind
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.ProgressComments'))
BEGIN
	DROP TABLE dbo.ProgressComments
END

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GoalActionsEditHistory'))
BEGIN
	DROP TABLE dbo.GoalActionsEditHistory
END

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.V_GoalActionsEditHistory'))
BEGIN
	DROP VIEW dbo.V_GoalActionsEditHistory
END

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.V_LDAPUsers'))
BEGIN
	DROP VIEW dbo.V_LDAPUsers
END

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.V_GoalActions'))
BEGIN
	DROP VIEW dbo.V_GoalActions
END

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GoalActions'))
BEGIN
	DROP TABLE dbo.GoalActions
END

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.Statuses'))
BEGIN
	DROP TABLE dbo.Statuses
END

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.Quarters'))
BEGIN
	DROP TABLE dbo.Quarters
END

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.Goals'))
BEGIN
	DROP TABLE dbo.Goals
END

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.StrategicBusinessUnits'))
BEGIN
	DROP TABLE dbo.StrategicBusinessUnits
END

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.LDAPUsers'))
BEGIN
	DROP TABLE dbo.LDAPUsers
END

-- make sure everything is rewound
IF ((SELECT COUNT(*) FROM sys.objects WHERE is_ms_shipped = 0) <> 0)
BEGIN
	PRINT 'You missed a rewind. Check SELECT * FROM sys.objects WHERE is_ms_shipped = 0 to find out what.'
	SET NOEXEC ON
END

-- create tables
CREATE TABLE dbo.LDAPUsers (
	UserLoginName VARCHAR(256) NOT NULL PRIMARY KEY,
	UserDisplayName VARCHAR(256) NOT NULL,
	ManagerLoginName VARCHAR(256) NULL,
	DateCreated DATETIME NOT NULL DEFAULT GETDATE(),
	DateModified DATETIME NOT NULL DEFAULT GETDATE()
)
GO

CREATE TRIGGER dbo.TRG_LDAPUsers_Verify
ON dbo.LDAPUsers
AFTER INSERT, UPDATE
AS
BEGIN
	-- ensure that only one user has a ManagerLoginName of NULL
	IF ((SELECT COUNT(*) FROM dbo.LDAPUsers WHERE ManagerLoginName IS NULL) > 1)
	BEGIN
		THROW 51000, 'Only one user can have a ManagerLoginName of NULL', 1
	END
END
GO

CREATE TRIGGER dbo.TRG_LDAPUsers_DateModified
ON dbo.LDAPUsers
AFTER UPDATE
AS
	UPDATE dbo.LDAPUsers
	SET DateModified = GETDATE()
	WHERE UserLoginName IN (SELECT DISTINCT UserLoginName FROM INSERTED)
GO

CREATE TABLE dbo.Statuses (
	StatusColor VARCHAR(16) NOT NULL UNIQUE CHECK (StatusColor IN ('Red', 'Yellow', 'Green')),
	StatusColorStyle VARCHAR(16) NOT NULL
)
GO

CREATE TABLE dbo.Quarters (
	Quarter CHAR(1) NOT NULL UNIQUE CHECK (Quarter IN ('1', '2', '3', '4'))
)
GO

CREATE TABLE dbo.StrategicBusinessUnits (
	StrategicBusinessUnitName VARCHAR(32) NOT NULL PRIMARY KEY,
	MasterStrategicBusinessUnitName VARCHAR(32)
)
GO

CREATE TRIGGER dbo.TRG_StrategicBusinessUnits_Verify
ON dbo.StrategicBusinessUnits
AFTER INSERT, UPDATE
AS
BEGIN
	-- ensure that MasterStrategicBusinessUnits are valid
	IF ((SELECT COUNT(MasterStrategicBusinessUnitName) FROM INSERTED WHERE MasterStrategicBusinessUnitName NOT IN (SELECT DISTINCT StrategicBusinessUnitName FROM dbo.StrategicBusinessUnits)) > 0)
	BEGIN
		THROW 51000, 'MasterStrategicBusinessUnitName values must either be NULL, or exist in the StrategicBusinessUnitName column', 1
	END
END
GO

CREATE TABLE dbo.Goals (
	Goal VARCHAR(32) NOT NULL PRIMARY KEY CHECK (
		Goal IN (
			'Decrease Expense',
			'Develop People',
			'Improve Quality',
			'Improve Safety',
			'Increase Revenue'
		)
	)
)
GO

CREATE TABLE dbo.GoalActions (
	GoalActionId INT NOT NULL IDENTITY PRIMARY KEY,
	StrategicBusinessUnitName VARCHAR(32) NOT NULL,
	Quarter CHAR(1) NOT NULL,
	Year INTEGER NOT NULL,
	Goal VARCHAR(32) NOT NULL,
	Objective NVARCHAR(256) NOT NULL,
	ActionStep NVARCHAR(MAX) NOT NULL,
	ActualOutcome VARCHAR(MAX) NULL,
	StatusColor VARCHAR(16) NOT NULL,
	UserLoginName VARCHAR(256) NOT NULL,
	DateCreated DATETIME NOT NULL DEFAULT GETDATE(),
	DateModified DATETIME NOT NULL DEFAULT GETDATE(),
	ParentGoalActionId INT NULL,
	LastEditedUserLoginName VARCHAR(256) NOT NULL,
	CONSTRAINT FK_GoalActions_Quarters
		FOREIGN KEY (Quarter) REFERENCES dbo.Quarters (Quarter),
	CONSTRAINT FK_GoalActions_Statuses
		FOREIGN KEY (StatusColor) REFERENCES dbo.Statuses (StatusColor),
	CONSTRAINT FK_GoalActions_Goals
		FOREIGN KEY (Goal) REFERENCES dbo.Goals (Goal),
	CONSTRAINT FK_GoalActions_StrategicBusinessUnits
		FOREIGN KEY (StrategicBusinessUnitName) REFERENCES dbo.StrategicBusinessUnits (StrategicBusinessUnitName),
	CONSTRAINT FK_GoalActions_LDAPUsers
		FOREIGN KEY (UserLoginName) REFERENCES dbo.LDAPUsers (UserLoginName)
)
GO

CREATE TRIGGER dbo.TRG_GoalActions_LastEditedUserLoginName
ON dbo.GoalActions
INSTEAD OF INSERT
AS
	INSERT INTO dbo.GoalActions (
		StrategicBusinessUnitName,
		Quarter,
		Year,
		Goal,
		Objective,
		ActionStep,
		ActualOutcome,
		StatusColor,
		UserLoginName,
		DateCreated,
		DateModified,
		ParentGoalActionId,
		LastEditedUserLoginName
	)
	SELECT
		StrategicBusinessUnitName,
		Quarter,
		Year,
		Goal,
		Objective,
		ActionStep,
		ActualOutcome,
		StatusColor,
		UserLoginName,
		DateCreated,
		DateModified,
		ParentGoalActionId,
		UserLoginName
	FROM INSERTED
GO

CREATE TABLE dbo.GoalActionsEditHistory (
	GoalActionId INT NOT NULL,
	StrategicBusinessUnitName VARCHAR(32) NOT NULL,
	Quarter CHAR(1) NOT NULL,
	Year INTEGER NOT NULL,
	Goal VARCHAR(32) NOT NULL,
	Objective NVARCHAR(256) NOT NULL,
	ActionStep NVARCHAR(MAX) NOT NULL,
	ActualOutcome VARCHAR(MAX) NULL,
	StatusColor VARCHAR(16) NOT NULL,
	UserLoginName VARCHAR(256) NOT NULL,
	DateCreated DATETIME NOT NULL,
	DateModified DATETIME NOT NULL,
	ParentGoalActionId INT NULL,
	LastEditedUserLoginName VARCHAR(256) NOT NULL
)
GO

CREATE TRIGGER dbo.TRG_GoalActions_Updated
ON dbo.GoalActions
AFTER UPDATE
AS
	INSERT INTO dbo.GoalActionsEditHistory
	SELECT
		*
	FROM DELETED
	
	UPDATE dbo.GoalActions
	SET DateModified = GETDATE()
	WHERE GoalActionId IN (SELECT DISTINCT GoalActionId FROM INSERTED)
GO

CREATE VIEW dbo.V_GoalActionsEditHistory
AS
	SELECT
		g.*,
		COALESCE(s.MasterStrategicBusinessUnitName, g.StrategicBusinessUnitName) AS MasterStrategicBusinessUnitName,
		st.StatusColorStyle
	FROM dbo.GoalActionsEditHistory AS g
	INNER JOIN dbo.StrategicBusinessUnits AS s
		ON g.StrategicBusinessUnitName = s.StrategicBusinessUnitName
	INNER JOIN dbo.Statuses AS st
		ON g.StatusColor = st.StatusColor

GO

CREATE TABLE dbo.ProgressComments (
	ProgressCommentId INT NOT NULL IDENTITY PRIMARY KEY,
	GoalActionId INT NOT NULL,
	ProgressCommentText NVARCHAR(MAX),
	UserLoginName VARCHAR(256) NOT NULL,
	DateCreated DATETIME NOT NULL DEFAULT GETDATE(),
	DateModified DATETIME NOT NULL DEFAULT GETDATE(),
	CONSTRAINT FK_ProgressComments_GoalActions
		FOREIGN KEY (GoalActionId) REFERENCES dbo.GoalActions (GoalActionId) ON DELETE CASCADE,
	CONSTRAINT FK_ProgressComments_LDAPUsers
		FOREIGN KEY (UserLoginName) REFERENCES dbo.LDAPUsers (UserLoginName)
)
GO

CREATE TRIGGER dbo.TRG_ProgressComments_Updated
ON dbo.ProgressComments
AFTER UPDATE
AS
	UPDATE dbo.ProgressComments
	SET DateModified = GETDATE()
	WHERE ProgressCommentId IN (SELECT DISTINCT ProgressCommentId FROM INSERTED)
GO

-- make sure that GoalActionsEditHistory and GoalActions have the same exact columns
IF (
	(
		SELECT
			COUNT(*) AS MismatchCount
		FROM (
			SELECT
				NULL AS Mismatch
			FROM INFORMATION_SCHEMA.COLUMNS AS ga
			LEFT JOIN (
				SELECT
					*
				FROM INFORMATION_SCHEMA.COLUMNS AS x
				WHERE x.TABLE_NAME = 'GoalActionsEditHistory'
			) AS gah
				ON ga.COLUMN_NAME = gah.COLUMN_NAME
					AND ga.DATA_TYPE = gah.DATA_TYPE
			WHERE ga.TABLE_NAME = 'GoalActions'
				AND gah.COLUMN_NAME IS NULL
			UNION ALL
			SELECT
				NULL AS Mismatch
			FROM INFORMATION_SCHEMA.COLUMNS AS gah
			LEFT JOIN (
				SELECT
					*
				FROM INFORMATION_SCHEMA.COLUMNS AS x
				WHERE x.TABLE_NAME = 'GoalActions'
			) AS ga
				ON ga.COLUMN_NAME = gah.COLUMN_NAME
					AND ga.DATA_TYPE = gah.DATA_TYPE
			WHERE ga.TABLE_NAME = 'GoalActionsEditHistory'
				AND gah.COLUMN_NAME IS NULL
		) AS q
	) > 0
)
BEGIN
	PRINT 'Columns and types in GoalActions and GoalActionsEditHistory must match.'
	SET NOEXEC ON
END
GO

-- lookup inserts
INSERT INTO dbo.Quarters
VALUES ('1'), ('2'), ('3'), ('4')

INSERT INTO dbo.Statuses
VALUES ('Red', 'MistyRose'), ('Yellow', 'LightYellow'), ('Green', 'Honeydew')

INSERT INTO dbo.Goals
VALUES
	('Decrease Expense'),
	('Develop People'),
	('Improve Quality'),
	('Improve Safety'),
	('Increase Revenue')

-- -- TODO: flesh this out
INSERT INTO dbo.StrategicBusinessUnits (
	StrategicBusinessUnitName,
	MasterStrategicBusinessUnitName
)
VALUES
	('Corporate', NULL),
	('Evans Distribution', NULL),
	('G&A', NULL),
	('Human Resources', NULL),
	('MIS', NULL),
	('Mgmt Serv', NULL),
	('Sales', NULL),
	('Transportation', NULL),
	('Value Added', NULL),
	('Warehouse', NULL),
	('Alden', 'Transportation'),
	('CDW', 'Sales'),
	('DPS', 'Warehouse'),
	('Evans Logistics', 'Transportation'),
	('Fort', 'Warehouse'),
	('Merchants', 'Transportation'),
	('Mt Elliott', 'Warehouse'),
	('Quality Sys', 'Corporate'),
	('Richmond', 'Warehouse'),
	('Seaway', 'Warehouse')
GO

-- view for GoalActions
CREATE VIEW dbo.V_GoalActions
AS
	SELECT
		g.*,
		COALESCE(s.MasterStrategicBusinessUnitName, g.StrategicBusinessUnitName) AS MasterStrategicBusinessUnitName,
		st.StatusColorStyle
	FROM dbo.GoalActions AS g
	INNER JOIN dbo.StrategicBusinessUnits AS s
		ON g.StrategicBusinessUnitName = s.StrategicBusinessUnitName
	INNER JOIN dbo.Statuses AS st
		ON g.StatusColor = st.StatusColor

GO

-- rollup view for LDAPUsers
CREATE VIEW dbo.V_LDAPUsers
AS
WITH DirectReports AS (
	SELECT
		0 AS Level,
		m.UserLoginName,
		m.UserDisplayName,
		m.ManagerLoginName,
		CONVERT(VARCHAR(256), NULL) AS ManagerDisplayName,
		CONVERT(VARCHAR(256), NULL) AS SuperManagerLoginName
	FROM dbo.LDAPUsers AS m
	WHERE m.ManagerLoginName IS NULL
	UNION ALL
	SELECT
		Level + 1,
		e.UserLoginName,
		e.UserDisplayName,
		e.ManagerLoginName,
		d.UserDisplayName AS ManagerDisplayName,
		d.ManagerLoginName AS SuperManagerLoginName
	FROM dbo.LDAPUsers AS e
	INNER JOIN DirectReports AS d
		ON e.ManagerLoginName = d.UserLoginName
), SuperDirectReports AS (
	SELECT
		d.*,
		u.UserDisplayName AS SuperManagerDisplayName
	FROM DirectReports AS d
	LEFT JOIN dbo.LDAPUsers AS u
		ON d.SuperManagerLoginName = u.UserLoginName
)
SELECT
	*
FROM SuperDirectReports AS sd
UNION
-- any users whose hierarchies aren't correct
SELECT
	-1 AS Level,
	u.UserLoginName,
	u.UserDisplayName,
	u.ManagerLoginName,
	CONVERT(VARCHAR(256), NULL) AS ManagerDisplayName,
	CONVERT(VARCHAR(256), NULL) AS SuperManagerLoginName,
	CONVERT(VARCHAR(256), NULL) AS SuperManagerDisplayName
FROM dbo.LDAPUsers AS u
WHERE u.UserDisplayName NOT IN (
	SELECT UserDisplayName FROM SuperDirectReports
)
